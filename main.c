#include <stdio.h>

int main(){

	float N1 = 2;
	float N2 = 3;
	float N3 = 0;

        realizaOperacoes(&N1, &N2, &N3);
	
	printf("Resultado das operações:\n Soma:%.2f \nDiferença:%.2f \n Produto:%.2f\n ", N1, N2, N3 );

   return 0;
}

void realizaOperacoes(float *a, float *b, float *c){

	float numero1 = *a;
	float numero2 = *b;
	float soma;
	float diferenca;
	float produto;

	soma = (numero1 + numero2);
	diferenca = (numero1 - numero2);
	produto = (numero1 * numero2);


	*a = soma;
	*b = diferenca;
	*c = produto;


}

